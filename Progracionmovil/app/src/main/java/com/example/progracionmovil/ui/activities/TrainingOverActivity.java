package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.progracionmovil.R;

public class TrainingOverActivity extends AppCompatActivity {

    String tiempo;
    TextView tvTiempo;
    Button volver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training_over);


        tiempo = getIntent().getExtras().getString("cronometro");
        tvTiempo = findViewById(R.id.tiempoEntrenamiento);
        volver = findViewById(R.id.volverEntrenamiento);

        tvTiempo.setText(tiempo);


        volver.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent Siguiente = new Intent(v.getContext(), HomeActivity.class);
                startActivity(Siguiente);
            }
        });
    }
}
