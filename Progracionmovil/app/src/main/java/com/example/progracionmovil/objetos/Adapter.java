package com.example.progracionmovil.objetos;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.progracionmovil.R;
import com.example.progracionmovil.ui.activities.ExcerciseDetailActivity;
import com.example.progracionmovil.ui.activities.HomeActivity;
import com.example.progracionmovil.ui.activities.SplashScreenActivity;
import com.example.progracionmovil.ui.fragments.ExerciseDetailFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.EjerciciosViewHolder> {

    List<Ejercicio> ejercicios;

    Context mcontext;

    public Adapter(Context context, List<Ejercicio> ejercicios) {
        this.ejercicios = ejercicios;
        this.mcontext = context;
    }


    @NonNull
    @Override
    public EjerciciosViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_exercise_recycler, parent, false);
        EjerciciosViewHolder holder = new EjerciciosViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final EjerciciosViewHolder holder, int position) {
        final Ejercicio ejercicio = ejercicios.get(position);
        holder.tvNombre.setText(ejercicio.getNombre().toUpperCase());

        //Cargamos la imagen
        Uri uri = Uri.parse(ejercicio.getImagen());
        Glide.with(mcontext.getApplicationContext()).load(uri).into(holder.ivEjercicio);

        holder.tvTipo.setText(ejercicio.getTipo().toUpperCase());
        holder.button.setTag(ejercicio.getNombre().toUpperCase());


        holder.button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), ExcerciseDetailActivity.class);
                intent.putExtra("titulo", ejercicio.getNombre().toUpperCase());
                intent.putExtra("descripcion", ejercicio.getDescripcion().toUpperCase());
                intent.putExtra("series", ejercicio.getNumeroCiclos().toUpperCase());
                intent.putExtra("reps", ejercicio.getNumeroRepeticiones().toUpperCase());
                intent.putExtra("gif", ejercicio.getImagen());
                intent.putExtra("requerimientos", ejercicio.getTipo().toUpperCase());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return ejercicios.size();
    }

    public static class EjerciciosViewHolder extends RecyclerView.ViewHolder {

        TextView tvNombre, tvDescripcion, tvImagen, tvCiclos, tvRepeticiones, tvTipo, tvDescEjercicio, txtView;
        ImageView ivEjercicio;
        Button button;

        public EjerciciosViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNombre = (TextView) itemView.findViewById(R.id.tvNombreEjercicio);
            ivEjercicio = (ImageView) itemView.findViewById(R.id.ivEjercicio);
            tvTipo = (TextView) itemView.findViewById(R.id.tvTipoEjercicio);
            button = itemView.findViewById(R.id.detallesEjercicio);
        }
    }

}
