package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progracionmovil.R;

public class Edad extends AppCompatActivity {

    EditText etEdad;

    String genero;
    String edad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edad);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Button button = findViewById(R.id.buttonSiguienteEdad);
        etEdad = findViewById(R.id.editTextEdad);

        //Obtengo valores usuario
        genero = getIntent().getExtras().getString("genero");


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (etEdad.getText().length()==0) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor ingrese su edad",
                            Toast.LENGTH_SHORT).show();
                }
                else{ // one of the radio buttons is checked
                    edad = etEdad.getText().toString();
                    Intent Siguiente = new Intent(v.getContext(), Peso.class);
                    Siguiente.putExtra("genero", genero);
                    Siguiente.putExtra("edad", edad);
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view){
        Intent Siguiente= new Intent(this, Peso.class);
        startActivity(Siguiente);
    }
    public void Anterior (View view){

        Intent Segundo = new Intent(this, Genero.class);
        startActivity(Segundo);
    }
}
