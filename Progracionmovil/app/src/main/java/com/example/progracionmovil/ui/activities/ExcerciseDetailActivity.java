package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.progracionmovil.R;
import com.example.progracionmovil.ui.fragments.LibraryFragment;

public class ExcerciseDetailActivity extends AppCompatActivity {

    TextView tvDescEjercicio, tvTituloEjercicio, tvCiclosEjercicio, tvRepsEjercicio, tvRequerimientosEjercicio;
    ImageView gifEjercicio;
    //Button bVolverDetalle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_excercise_detail);

        tvTituloEjercicio = (TextView)findViewById(R.id.tvTituloDetalleEjercicio) ;
        tvDescEjercicio = (TextView)findViewById(R.id.tvDescripcionEjercicio) ;
        tvCiclosEjercicio = (TextView)findViewById(R.id.tvCiclosEjercicio) ;
        tvRepsEjercicio = (TextView)findViewById(R.id.tvRepeticionesEjercicio) ;
        gifEjercicio = (ImageView)findViewById(R.id.gifEjercicio);
        tvRequerimientosEjercicio = findViewById(R.id.tvRequerimientoEjercicio);
        //bVolverDetalle = findViewById(R.id.bVolverDetalle);

        String titulo = getIntent().getExtras().getString("titulo");
        String descripcion = getIntent().getExtras().getString("descripcion");
        String series = getIntent().getExtras().getString("series");
        String reps = getIntent().getExtras().getString("reps");
        String gif = getIntent().getExtras().getString("gif");
        String requerimientos = getIntent().getExtras().getString("requerimientos");

        tvTituloEjercicio.setText(titulo);
        tvDescEjercicio.setText(descripcion);
        tvCiclosEjercicio.setText(series);
        tvRepsEjercicio.setText(reps);
        tvRequerimientosEjercicio.setText(requerimientos);

        Uri uri = Uri.parse(gif);
        Glide.with(this).load(uri).into(gifEjercicio);

//        bVolverDetalle.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                Intent Siguiente = new Intent(v.getContext(), HomeActivity.class);
//                Siguiente.putExtra("valor", "si");
//                startActivity(Siguiente);
//            }
//        });
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
    }
}
