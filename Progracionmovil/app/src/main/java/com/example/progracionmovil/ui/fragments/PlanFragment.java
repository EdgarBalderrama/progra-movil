package com.example.progracionmovil.ui.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progracionmovil.R;
import com.example.progracionmovil.objetos.Adapter;
import com.example.progracionmovil.objetos.Ejercicio;
import com.example.progracionmovil.objetos.ExcerciseAdapter;
import com.example.progracionmovil.objetos.FirebaseReferences;
import com.example.progracionmovil.objetos.Plan;
import com.example.progracionmovil.objetos.Usuario;
import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlanFragment extends Fragment {

    ProgressBar mProgressBar;
    View mView;
    TextView mTextView, pruebaPlan, pruebaPlan1;
    int mProgressStatus = 0;
    Handler mHandler = new Handler();

    ExcerciseAdapter adapter;

    RecyclerView mRecyclerView;
    List<Ejercicio> ejercicios;
    List<Plan> planes;

    String plan;

    private DatabaseReference mDatabase;
    private DatabaseReference mDatabase1;

    Context context;

    public PlanFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_plan, container, false);

        //Estaba en el on start




        context = view.getContext();


        return view;
    }

    @Override
    public void onStart() {
        super.onStart();



        //Carga del progressBar
        mProgressBar = (ProgressBar) getView().findViewById(R.id.pbPlan);
        mView = (View) getView().findViewById(R.id.viewPlan);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mProgressStatus < 100){
                    mProgressStatus++;
                    android.os.SystemClock.sleep(30);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setProgress(mProgressStatus);
                        }
                    });
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        mView.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).start();

        pruebaPlan = getView().findViewById(R.id.tvPruebaPlan);
        pruebaPlan1 = getView().findViewById(R.id.tvPruebaPlan1);










        mDatabase = FirebaseDatabase.getInstance().getReference().child("usuario");
        Query lastQuery = mDatabase.orderByKey().limitToLast(1);
        lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for (DataSnapshot snapshots : snapshot.getChildren()
                ) {
                    Usuario usuario = snapshots.getValue(Usuario.class);
                    plan = usuario.getPlan();
                }

                pruebaPlan.setText(plan);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        //LEER DATOS DE LA BD
        mRecyclerView = getView().findViewById(R.id.rvPlan);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mRecyclerView.setLayoutManager(layoutManager);

        ejercicios = new ArrayList<>();
        planes = new ArrayList<>();




        //Llenamos lista ejercicios
        FirebaseDatabase database1 = FirebaseDatabase.getInstance();
        final DatabaseReference myRef1 = database1.getReference(FirebaseReferences.EJERCICIO_REFERENCE);
        myRef1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ejercicios.removeAll(ejercicios);
                for (DataSnapshot snapshots: snapshot.getChildren()
                ) {
                    Ejercicio ejercicio = snapshots.getValue(Ejercicio.class);
                    ejercicios.add(ejercicio);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        if(plan == null || plan == ""){
            pruebaPlan1.setText("vacios choco");
        } else{
            pruebaPlan1.setText(plan);
        }

        //Llenamos lista de planes

        mDatabase1 = FirebaseDatabase.getInstance().getReference().child(pruebaPlan.getText().toString());

        //FirebaseDatabase database = FirebaseDatabase.getInstance();
        adapter = new ExcerciseAdapter(getContext(), ejercicios, planes);
        mRecyclerView.setAdapter(adapter);
        //DatabaseReference myRef = database.getReference(FirebaseReferences.PLAN_BD_REFERENCE);
        mDatabase1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                planes.removeAll(planes);
                for (DataSnapshot snapshots: snapshot.getChildren()
                ) {
                    Plan plan = snapshots.getValue(Plan.class);
                    planes.add(plan);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
