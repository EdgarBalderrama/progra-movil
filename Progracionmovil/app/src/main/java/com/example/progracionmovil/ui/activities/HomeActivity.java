package com.example.progracionmovil.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.progracionmovil.MainActivity;
import com.example.progracionmovil.R;
import com.example.progracionmovil.objetos.Ejercicio;
import com.example.progracionmovil.objetos.FirebaseReferences;
import com.example.progracionmovil.ui.fragments.DiaryFragment;
import com.example.progracionmovil.ui.fragments.LibraryFragment;
import com.example.progracionmovil.ui.fragments.OwnWorkoutsFragment;
import com.example.progracionmovil.ui.fragments.PlanFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    BottomNavigationView bottomNav;
    Button buttonPrueba;
    String valor;
    TextView tvGenero;

    //Variables de ingreso de usuario
    String genero, edad, peso, altura, objetivo, entrenamiento, nivel, key, imcUsuario, plan;


    //Int para IMC
    int pesoIMC, alturaIMC, imc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Obtención de variables de layout
        tvGenero = findViewById(R.id.tvInsertGender);


        showSelectedFragment(new DiaryFragment());


            //Obtencion de datos de usuario
            // Creamos un nuevo Bundle
            Bundle bundle = getIntent().getExtras();
            if (bundle != null)
            {
                // Variables de ingreso de usuario
                genero = getIntent().getExtras().getString("genero");
                edad = getIntent().getExtras().getString("edad");
                peso = getIntent().getExtras().getString("peso");
                altura = getIntent().getExtras().getString("altura");
                objetivo = getIntent().getExtras().getString("objetivo");
                entrenamiento = getIntent().getExtras().getString("entrenamiento");
                nivel = getIntent().getExtras().getString("nivel");

                //Calculamos IMC
                alturaIMC = Integer.parseInt(altura);
                pesoIMC = Integer.parseInt(peso);
                imc = pesoIMC / ((alturaIMC/100)*(alturaIMC/100));

                altura = altura.concat(" cm");
                peso = peso.concat(" kg");
                edad = edad.concat(" años");
                imcUsuario = imc + "";

                //Generamos plan
                if (objetivo.equals("Aumentar masa muscular")){
                    if (entrenamiento.equals("Peso corporal")){
                        plan = "Hipertrofia calistenica";
                    } else if(entrenamiento.equals("Mancuernas")){
                        plan = "Hipertrofia de gimnasio";
                    } else if(entrenamiento.equals("Peso corporal y mancuernas")){
                        plan = "Hipertrofia completa";
                    }
                } else if (objetivo.equals("Disminuir grasa corporal")){
                    if (entrenamiento.equals("Peso corporal")){
                        plan = "Definicion calistenica";
                    } else if(entrenamiento.equals("Mancuernas")){
                        plan = "Definicion de gimnasio";
                    } else if(entrenamiento.equals("Peso corporal y mancuernas")){
                        plan = "Definicion completa";
                    }
                } else {
                    plan = "Super plan";
                }

                FirebaseDatabase database = FirebaseDatabase.getInstance();
                final DatabaseReference myRef = database.getReference(FirebaseReferences.USUARIO_REFERENCE);


                Map<String, Object> datosUsuario = new HashMap<>();
                datosUsuario.put("genero", genero);
                datosUsuario.put("edad", edad);
                datosUsuario.put("peso", peso);
                datosUsuario.put("altura", altura);
                datosUsuario.put("objetivo", objetivo);
                datosUsuario.put("entrenamiento", entrenamiento);
                datosUsuario.put("imcUsuario", imcUsuario);
                datosUsuario.put("plan", plan);
                datosUsuario.put("nivelActual", nivel);

                myRef.push().setValue(datosUsuario);
                key = myRef.push().getKey();

                //Mandamos al fragment
                bundle.putString("key", key);

                bundle.putString("genero", genero);
                bundle.putString("edad", edad);
                bundle.putString("peso", peso);
                bundle.putString("altura", altura);
                bundle.putString("objetivo", objetivo);
                bundle.putString("entrenamiento", entrenamiento);
                bundle.putString("nivel", nivel);
                bundle.putString("plan", plan);
                bundle.putString("imcUsuario", imcUsuario);

                //Colocamos este nuevo Bundle como argumento en el fragmento.
                DiaryFragment newFragment = new DiaryFragment();
                newFragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragContent, newFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }

            bottomNav = (BottomNavigationView) findViewById(R.id.bnvMenu);

            bottomNav.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                if (menuItem.getItemId() == R.id.navHomeFragment){
                    showSelectedFragment(new DiaryFragment());
                }
                if (menuItem.getItemId() == R.id.navPlanFragment){
                    showSelectedFragment(new PlanFragment());
                }
//                if (menuItem.getItemId() == R.id.navOwnFragment){
//                    showSelectedFragment(new OwnWorkoutsFragment());
//                }
                if (menuItem.getItemId() == R.id.navLibraryFragment){

                    showSelectedFragment(new LibraryFragment());
                }

                return true;
            }
        });





    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        valor = getIntent().getExtras().getString("valor");
//        if (valor.equals("si")){
//
//            valor = "no";
//            showSelectedFragment(new LibraryFragment());
//        }
//    }

    public void showSelectedFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.fragContent, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }


}
