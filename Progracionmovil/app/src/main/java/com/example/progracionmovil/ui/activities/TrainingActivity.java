package com.example.progracionmovil.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progracionmovil.R;
import com.example.progracionmovil.objetos.Adapter;
import com.example.progracionmovil.objetos.Ejercicio;
import com.example.progracionmovil.objetos.FirebaseReferences;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TrainingActivity extends AppCompatActivity {
    private Chronometer chronometer;
    private long pauseOffset;
    private boolean running;

    Button buttonPrueba;
    RecyclerView mRecyclerView;
    List<Ejercicio> ejercicios;
    Adapter adapter;


    ProgressBar mProgressBar;
    TextView mTextView;
    int mProgressStatus = 0;
    Handler mHandler = new Handler();
    View mView;
    boolean isActive = false;
    int i = 0;
    Intent x;

    String planUsuario;

    Button terminarEntrenamiento;


    private Handler handler = new Handler();
    private int progressStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);

    }

    @Override
    protected void onStart() {
        super.onStart();

        chronometer = findViewById(R.id.chronometer);
        chronometer.setFormat("Tiempo: %s");
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
//                if ((SystemClock.elapsedRealtime() - chronometer.getBase()) >= 10000) {
//                    chronometer.setBase(SystemClock.elapsedRealtime());
//                    Toast.makeText(TrainingActivity.this, "Bing!", Toast.LENGTH_SHORT).show();
//                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //LEER DATOS DE LA BD

        terminarEntrenamiento = findViewById(R.id.terminarEntrenamiento);



        mRecyclerView = findViewById(R.id.rvTraining);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(layoutManager);

        ejercicios = new ArrayList<>();


        planUsuario = getIntent().getExtras().getString("planUsuario");

        if (planUsuario.equals("15")){
            planUsuario = "4";
        } else if (planUsuario.equals("16")){
            planUsuario = "13";
        } else if (planUsuario.equals("18")){
            planUsuario = "1";
        }

        DatabaseReference database = FirebaseDatabase.getInstance().getReference();
        adapter = new Adapter(this,ejercicios);
        mRecyclerView.setAdapter(adapter);
        //Query lastQuery = myRef.orderByKey().limitToLast(1);
        Query query = database.child("ejercicio").orderByChild("plan").equalTo(planUsuario);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ejercicios.removeAll(ejercicios);
                for (DataSnapshot snapshots: snapshot.getChildren()
                ) {
                    Ejercicio ejercicio = snapshots.getValue(Ejercicio.class);
                    ejercicios.add(ejercicio);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        terminarEntrenamiento.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent Siguiente = new Intent(v.getContext(), TrainingOverActivity.class);
                Siguiente.putExtra("cronometro", chronometer.getText());
                startActivity(Siguiente);
            }
        });
    }

    public void startChronometer(View v) {
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffset);
            chronometer.start();
            running = true;
        }
    }
    public void pauseChronometer(View v) {
        if (running) {
            chronometer.stop();
            pauseOffset = SystemClock.elapsedRealtime() - chronometer.getBase();
            running = false;
        }
    }
    public void resetChronometer(View v) {
        chronometer.setBase(SystemClock.elapsedRealtime());
        pauseOffset = 0;
    }
}
