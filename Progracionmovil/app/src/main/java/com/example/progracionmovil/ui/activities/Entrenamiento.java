package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.progracionmovil.R;

public class Entrenamiento extends AppCompatActivity {

    RadioGroup rbEntrenamiento;
    RadioButton pesoCorporal;
    RadioButton mancuernas;
    RadioButton mixto;

    //Variables de ingreso de usuario
    String genero;
    String edad;
    String peso;
    String altura;
    String objetivo;

    String entrenamiento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrenamiento);
    }

    @Override
    protected void onResume() {
        super.onResume();
        rbEntrenamiento = findViewById(R.id.radioGroupObjetivo);
        pesoCorporal = findViewById(R.id.pesoCorporal);
        mancuernas = findViewById(R.id.mancuernas);
        mixto = findViewById(R.id.mixto);


        final Button button = findViewById(R.id.buttonSiguienteObjetivo);


        // Variables de ingreso de usuario
        genero = getIntent().getExtras().getString("genero");
        edad = getIntent().getExtras().getString("edad");
        peso = getIntent().getExtras().getString("peso");
        altura = getIntent().getExtras().getString("altura");
        objetivo = getIntent().getExtras().getString("objetivo");


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (rbEntrenamiento.getCheckedRadioButtonId() == -1) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor seleccione la forma de entrenamiento",
                            Toast.LENGTH_SHORT).show();
                }
                else{ // one of the radio buttons is checked
                    if (pesoCorporal.isChecked()){
                        entrenamiento = "Peso corporal";
                    }
                    else if (mancuernas.isChecked()){
                        entrenamiento = "Mancuernas";
                    }
                    else{
                        entrenamiento = "Peso corporal y mancuernas";
                    }
                    Intent Siguiente = new Intent(v.getContext(), Nivel.class);
                    Siguiente.putExtra("genero", genero);
                    Siguiente.putExtra("edad", edad);
                    Siguiente.putExtra("peso", peso);
                    Siguiente.putExtra("altura", altura);
                    Siguiente.putExtra("objetivo", objetivo);
                    Siguiente.putExtra("entrenamiento", entrenamiento);
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view){
        Intent Siguiente= new Intent(this, Nivel.class);
        startActivity(Siguiente);
    }
    public void Anterior (View view){
        Intent Segundo = new Intent(this, Objetivo.class);
        startActivity(Segundo);
    }
}
