package com.example.progracionmovil.ui.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.progracionmovil.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserDetailDialogFragment extends Fragment {

    public UserDetailDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_user_detail_dialog, container, false);
    }
}
