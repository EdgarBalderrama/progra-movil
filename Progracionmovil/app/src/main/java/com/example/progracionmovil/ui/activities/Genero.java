package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.progracionmovil.MainActivity;
import com.example.progracionmovil.R;

public class Genero extends AppCompatActivity {

    RadioGroup rbGenero;
    RadioButton masculino;
    RadioButton fememino;
    String genero;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genero);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Button button = findViewById(R.id.buttonSiguienteGenero);
        rbGenero = findViewById(R.id.radioGroupGenero);
        masculino = findViewById(R.id.radioButtonHombre);
        fememino = findViewById(R.id.radioButtonMujer);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (rbGenero.getCheckedRadioButtonId() == -1) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor seleccione una opcion",
                            Toast.LENGTH_SHORT).show();
                } else { // one of the radio buttons is checked
                    if (masculino.isChecked()){
                        genero = "Hombre";
                    }
                    else {
                        genero = "Mujer";
                    }
                    Intent Siguiente = new Intent(v.getContext(), Edad.class);
                    Siguiente.putExtra("genero", genero);
                    SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("bandera", true);
                    editor.commit();
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view) {

        if (rbGenero.getCheckedRadioButtonId() == -1) { // no radio buttons are checked }
            Toast.makeText(this, "Porfavor seleccione una opción", Toast.LENGTH_LONG).show();
        } else { // one of the radio buttons is checked
            Intent Siguiente = new Intent(this, Edad.class);
            SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("bandera", true);
            editor.commit();
            startActivity(Siguiente);
        }
    }

    public void Anterior(View view) {
        Intent Segundo = new Intent(this, MainActivity.class);
        startActivity(Segundo);
    }
}
