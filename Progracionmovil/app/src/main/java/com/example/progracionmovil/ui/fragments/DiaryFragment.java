package com.example.progracionmovil.ui.fragments;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.progracionmovil.MainActivity;
import com.example.progracionmovil.R;
import com.example.progracionmovil.objetos.Ejercicio;
import com.example.progracionmovil.objetos.FirebaseReferences;
import com.example.progracionmovil.objetos.Usuario;
import com.example.progracionmovil.ui.activities.Edad;
import com.example.progracionmovil.ui.activities.HomeActivity;
import com.example.progracionmovil.ui.activities.ProfileSettings;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * A simple {@link Fragment} subclass.
 */
public class DiaryFragment extends Fragment {

    String key;

    //Variables para cargar layout
    String edad, peso, altura, objetivo, entrenamiento, nivel, plan, imcUsuario;
    String genero = "a";

    //Layout
    TextView tvGenero, tvObjetivo, tvPlan, tvPeso, tvAltura, tvEntrenamiento, tvNivel, tvEdad;
    ImageView profileImage;



    private DatabaseReference mDatabase;

    public DiaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_diary, container, false);


        tvGenero = view.findViewById(R.id.tvInsertGender);
        tvObjetivo = view.findViewById(R.id.tvInsertObjective);
        tvPeso = view.findViewById(R.id.tvInsertWeight);
        tvAltura = view.findViewById(R.id.tvInsertHeight);
        tvEntrenamiento = view.findViewById(R.id.tvInsertTrainings);
        tvEdad = view.findViewById(R.id.tvInsertAge);

        tvPlan = view.findViewById(R.id.tvInsertPlan);
        tvNivel = view.findViewById(R.id.tvInsertNivel);

        profileImage = view.findViewById(R.id.profile_image);



        if (getArguments() != null) {
            //Creamos un key fijo del ID del usuario
            key = getArguments().getString("key");
            FirebaseReferences.ID_REFERENCE = key;

            //Otros valores
            genero = getArguments().getString("genero");
            FirebaseReferences.GENERO_REFERENCE = genero;
            edad = getArguments().getString("edad");
            FirebaseReferences.EDAD_REFERENCE = edad;
            peso = getArguments().getString("peso");
            FirebaseReferences.PESO_REFERENCE = peso;
            altura = getArguments().getString("altura");
            FirebaseReferences.ALTURA_REFERENCE = altura;
            objetivo = getArguments().getString("objetivo");
            FirebaseReferences.OBJETIVO_REFERENCE = objetivo;
            entrenamiento = getArguments().getString("entrenamiento");
            FirebaseReferences.ENTRENAMIENTOS_REFERENCE = entrenamiento;
            nivel = getArguments().getString("nivel");
            FirebaseReferences.NIVEL_REFERENCE = nivel;


            plan = getArguments().getString("plan");
            FirebaseReferences.PLAN_REFERENCE = plan;
            imcUsuario = getArguments().getString("imcUsuario");
            FirebaseReferences.IMC_REFERENCE = imcUsuario;


        }
        tvPlan.setText(FirebaseReferences.PLAN_REFERENCE);
        tvGenero.setText(FirebaseReferences.GENERO_REFERENCE);
        tvEdad.setText(FirebaseReferences.EDAD_REFERENCE);
        tvPeso.setText(FirebaseReferences.PESO_REFERENCE);
        tvAltura.setText(FirebaseReferences.ALTURA_REFERENCE);
        tvObjetivo.setText(FirebaseReferences.OBJETIVO_REFERENCE);
        tvEntrenamiento.setText(FirebaseReferences.ENTRENAMIENTOS_REFERENCE);
        tvNivel.setText(FirebaseReferences.IMC_REFERENCE);

//
//
//
//        tvGenero.setText(genero);
//        tvObjetivo.setText(objetivo);
//        tvPeso.setText(peso);
//        tvAltura.setText(altura);
//        tvObjetivo.setText(objetivo);
//        tvEntrenamiento.setText(entrenamiento);
//        tvNivel.setText(nivel);
//        tvEdad.setText(edad);


        ImageView btnLanzarActivity = (ImageView) view.findViewById(R.id.ivProfileSettings);
        btnLanzarActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Crear fragmento de tu clase
                Fragment fragment = new UserSettings();
                // Obtener el administrador de fragmentos a través de la actividad
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                // Definir una transacción
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                // Remplazar el contenido principal por el fragmento
                fragmentTransaction.replace(R.id.fragContent, fragment);
                fragmentTransaction.addToBackStack(null);
                // Cambiar
                fragmentTransaction.commit();
            }
        });

        return view;

    }


    @Override
    public void onResume() {
        super.onResume();


        //Leer bd
        if (genero.equals("a")) {
            mDatabase = FirebaseDatabase.getInstance().getReference();
            mDatabase.child("usuario").child(FirebaseReferences.ID_REFERENCE).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    for (DataSnapshot snapshots : snapshot.getChildren()
                    ) {
                        Usuario usuario = snapshots.getValue(Usuario.class);
//                        altura = snapshots.child("altura").getValue().toString();
//                        edad = snapshots.child("edad").getValue().toString();
//                        entrenamiento = snapshots.child("entrenamiento").getValue().toString();
//                        genero = snapshots.child("genero").getValue().toString();
//                        nivel = snapshots.child("nivelActual").getValue().toString();
//                        objetivo = snapshots.child("objetivo").getValue().toString();
//                        peso = snapshots.child("peso").getValue().toString();

                        altura = usuario.getAltura();
                        edad = usuario.getEdad();
                        entrenamiento = usuario.getEntrenamiento();
                        genero = usuario.getGenero();
                        nivel = usuario.getNivel();
                        objetivo = usuario.getObjetivo();
                        peso = usuario.getPeso();
                        plan = usuario.getPlan();
                        imcUsuario = usuario.getImcUsuario();

                        tvAltura.setText(altura);
                        tvEdad.setText(edad);
                        tvEntrenamiento.setText(entrenamiento);
                        tvGenero.setText(genero);
                        tvNivel.setText(nivel);
                        tvObjetivo.setText(objetivo);
                        tvPeso.setText(peso);
                        tvNivel.setText(imcUsuario);
                        tvPlan.setText(plan);

                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }

    }
}
