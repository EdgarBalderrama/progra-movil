package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.progracionmovil.R;

public class Nivel extends AppCompatActivity {

    RadioGroup rbNivel;
    RadioButton principiante;
    RadioButton intermedio;
    RadioButton avanzado;

    //Variables de ingreso de usuario
    String genero;
    String edad;
    String peso;
    String altura;
    String objetivo;
    String entrenamiento;
    String nivel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel);
    }

    @Override
    protected void onResume() {
        super.onResume();
        rbNivel = findViewById(R.id.radioGroupNivel);
        principiante = findViewById(R.id.principiante);
        intermedio = findViewById(R.id.intermedio);
        avanzado = findViewById(R.id.avanzado);

        final Button button = findViewById(R.id.buttonSiguienteNivel);

        // Variables de ingreso de usuario
        Bundle bundle = getIntent().getExtras();
        genero = getIntent().getExtras().getString("genero");
        edad = getIntent().getExtras().getString("edad");
        peso = getIntent().getExtras().getString("peso");
        altura = getIntent().getExtras().getString("altura");
        objetivo = getIntent().getExtras().getString("objetivo");
        entrenamiento = getIntent().getExtras().getString("entrenamiento");

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (rbNivel.getCheckedRadioButtonId() == -1) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor seleccione su nivel actual",
                            Toast.LENGTH_SHORT).show();
                }
                else{ // one of the radio buttons is checked
                    if (principiante.isChecked()){
                        nivel = "Principiante";
                    }
                    else if (intermedio.isChecked()){
                        nivel = "Intermedio";
                    }
                    else{
                        nivel = "Avanzado";
                    }
                    Intent Siguiente = new Intent(v.getContext(), HomeActivity.class);

                    //Envio de datos a siguiente activity
                    Siguiente.putExtra("genero", genero);
                    Siguiente.putExtra("edad", edad);
                    Siguiente.putExtra("peso", peso);
                    Siguiente.putExtra("altura", altura);
                    Siguiente.putExtra("objetivo", objetivo);
                    Siguiente.putExtra("entrenamiento", entrenamiento);
                    Siguiente.putExtra("nivel", nivel);

                    SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("bandera", true);
                    editor.commit();
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view){
        Intent Siguiente= new Intent(this, HomeActivity.class);
        startActivity(Siguiente);
    }
    public void Anterior (View view){
        Intent Segundo = new Intent(this, Entrenamiento.class);
        startActivity(Segundo);
    }
}
