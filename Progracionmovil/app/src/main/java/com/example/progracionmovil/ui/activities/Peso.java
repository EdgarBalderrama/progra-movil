package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progracionmovil.R;

public class Peso extends AppCompatActivity {

    EditText tvPeso;
    String genero;
    String edad;

    String peso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peso);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Button button = findViewById(R.id.buttonSiguientePeso);
        tvPeso = findViewById(R.id.editTextPeso);

        // Variables de ingreso de usuario
        genero = getIntent().getExtras().getString("genero");
        edad = getIntent().getExtras().getString("edad");


        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (tvPeso.getText().length()==0) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor ingrese su peso",
                            Toast.LENGTH_SHORT).show();
                }
                else{ // one of the radio buttons is checked
                    peso = tvPeso.getText().toString();
                    Intent Siguiente = new Intent(v.getContext(), Altura.class);
                    Siguiente.putExtra("genero", genero);
                    Siguiente.putExtra("edad", edad);
                    Siguiente.putExtra("peso", peso);
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view){
        Intent Siguiente= new Intent(this, Altura.class);
        startActivity(Siguiente);
    }
    public void Anterior (View view){

        Intent Segundo = new Intent(this, Edad.class);
        startActivity(Segundo);
    }
}
