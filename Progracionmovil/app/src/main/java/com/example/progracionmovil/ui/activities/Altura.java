package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.progracionmovil.R;

public class Altura extends AppCompatActivity {

    EditText tvAltura;


    //Variables de ingreso de usuario
    String genero;
    String edad;
    String peso;

    String altura;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_altura);
    }

    @Override
    protected void onResume() {
        super.onResume();
        final Button button = findViewById(R.id.buttonSiguienteAltura);
        tvAltura = findViewById(R.id.editTextAltura);

        // Variables de ingreso de usuario
        Bundle bundle = getIntent().getExtras();
            genero = getIntent().getExtras().getString("genero");
            edad = getIntent().getExtras().getString("edad");
            peso = getIntent().getExtras().getString("peso");

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (tvAltura.getText().length()==0) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor ingrese su altura",
                            Toast.LENGTH_SHORT).show();
                }
                else{ // one of the radio buttons is checked
                    altura = tvAltura.getText().toString();
                    Intent Siguiente = new Intent(v.getContext(), Objetivo.class);
                    Siguiente.putExtra("genero", genero);
                    Siguiente.putExtra("edad", edad);
                    Siguiente.putExtra("peso", peso);
                    Siguiente.putExtra("altura", altura);
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view){
        Intent Siguiente= new Intent(this, Objetivo.class);
        startActivity(Siguiente);
    }
    public void Anterior (View view){
        Intent Segundo = new Intent(this, Peso.class);
        startActivity(Segundo);
    }
}
