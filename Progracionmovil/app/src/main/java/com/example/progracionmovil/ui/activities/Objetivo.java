package com.example.progracionmovil.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.progracionmovil.R;

public class Objetivo extends AppCompatActivity {

    RadioGroup rbObjetivo;
    RadioButton musculo;
    RadioButton grasa;

    //Variables de ingreso de usuario
    String genero;
    String edad;
    String peso;
    String altura;

    String objetivo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_objetivo);
    }

    @Override
    protected void onResume() {
        super.onResume();
        rbObjetivo = findViewById(R.id.radioGroupObjetivo);
        musculo = findViewById(R.id.musculo);
        grasa = findViewById(R.id.grasa);

        final Button button = findViewById(R.id.buttonSiguienteObjetivo);

        // Variables de ingreso de usuario
        genero = getIntent().getExtras().getString("genero");
        edad = getIntent().getExtras().getString("edad");
        peso = getIntent().getExtras().getString("peso");
        altura = getIntent().getExtras().getString("altura");

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (rbObjetivo.getCheckedRadioButtonId() == -1) { // no radio buttons are checked }
                    Toast.makeText(v.getContext(), "Porfavor seleccione su objetivo",
                            Toast.LENGTH_SHORT).show();
                }
                else{ // one of the radio buttons is checked
                    if (musculo.isChecked()){
                        objetivo = "Aumentar masa muscular";
                    }
                    else {
                        objetivo = "Disminuir grasa corporal";
                    }
                    Intent Siguiente = new Intent(v.getContext(), Entrenamiento.class);
                    Siguiente.putExtra("genero", genero);
                    Siguiente.putExtra("edad", edad);
                    Siguiente.putExtra("peso", peso);
                    Siguiente.putExtra("altura", altura);
                    Siguiente.putExtra("objetivo", objetivo);
                    startActivity(Siguiente);
                }
            }
        });
    }

    public void Siguiente(View view){
        Intent Siguiente= new Intent(this, Entrenamiento.class);
        startActivity(Siguiente);
    }
    public void Anterior (View view){
        Intent Segundo = new Intent(this, Altura.class);
        startActivity(Segundo);
    }
}
