package com.example.progracionmovil.ui.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.progracionmovil.R;
import com.example.progracionmovil.objetos.Adapter;
import com.example.progracionmovil.objetos.Ejercicio;
import com.example.progracionmovil.objetos.FirebaseReferences;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class LibraryFragment extends Fragment {

    Button buttonPrueba;
    RecyclerView mRecyclerView;
    List<Ejercicio> ejercicios;
    Adapter adapter;


    ProgressBar mProgressBar;
    TextView mTextView;
    int mProgressStatus = 0;
    Handler mHandler = new Handler();
    View mView;


    boolean isActive = false;
    int i = 0;
    Intent x;


    private Handler handler = new Handler();
    private int progressStatus = 0;

    public LibraryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_library, container, false);

        //Carga del progressBar
        mProgressBar = (ProgressBar) view.findViewById(R.id.pblibrary);
        mView = (View) view.findViewById(R.id.viewLibrary);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (mProgressStatus < 100){
                    mProgressStatus++;
                    android.os.SystemClock.sleep(50);
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgressBar.setProgress(mProgressStatus);
                        }
                    });
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setVisibility(View.INVISIBLE);
                        mView.setVisibility(View.INVISIBLE);
                    }
                });
            }
        }).start();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        //LEER DATOS DE LA BD
        mRecyclerView = getView().findViewById(R.id.rvLibrary);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());

        mRecyclerView.setLayoutManager(layoutManager);

        ejercicios = new ArrayList<>();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        adapter = new Adapter(getContext(),ejercicios);
        mRecyclerView.setAdapter(adapter);
        DatabaseReference myRef = database.getReference(FirebaseReferences.EJERCICIO_REFERENCE);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ejercicios.removeAll(ejercicios);
                for (DataSnapshot snapshots: snapshot.getChildren()
                ) {
                    Ejercicio ejercicio = snapshots.getValue(Ejercicio.class);
                    ejercicios.add(ejercicio);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}
