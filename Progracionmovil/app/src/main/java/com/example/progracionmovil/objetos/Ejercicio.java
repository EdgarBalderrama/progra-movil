package com.example.progracionmovil.objetos;

public class Ejercicio {
    private String descripcion, imagen, nombre;
    private String numeroCiclos, numeroRepeticiones;
    private String tipo, plan;

    public Ejercicio(String descripcion, String imagen, String nombre, String numeroCiclos, String numeroRepeticiones, String tipo, String plan) {
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.nombre = nombre;
        this.numeroCiclos = numeroCiclos;
        this.numeroRepeticiones = numeroRepeticiones;
        this.tipo = tipo;
        this.plan = plan;
    }

    public Ejercicio() {
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumeroCiclos() {
        return numeroCiclos;
    }

    public void setNumeroCiclos(String numeroCiclos) {
        this.numeroCiclos = numeroCiclos;
    }

    public String getNumeroRepeticiones() {
        return numeroRepeticiones;
    }

    public void setNumeroRepeticiones(String numeroRepeticiones) {
        this.numeroRepeticiones = numeroRepeticiones;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }
}
