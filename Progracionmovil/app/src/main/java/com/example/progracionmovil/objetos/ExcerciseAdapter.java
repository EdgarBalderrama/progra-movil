package com.example.progracionmovil.objetos;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.progracionmovil.R;
import com.example.progracionmovil.ui.activities.ExcerciseDetailActivity;
import com.example.progracionmovil.ui.activities.TrainingActivity;

import java.util.List;

public class ExcerciseAdapter extends RecyclerView.Adapter<ExcerciseAdapter.PlanesViewHolder>{

    private List<Ejercicio> ejercicios;
    private List<Plan> planes;
    String planUsuario;

    Context mcontext;


    public ExcerciseAdapter(Context context, List<Ejercicio> ejercicios, List<Plan> planes) {
        this.ejercicios = ejercicios;
        this.planes = planes;
        this.mcontext = context;
    }

    @NonNull
    @Override
    public PlanesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_plan_recycler, parent, false);
        PlanesViewHolder holder = new PlanesViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlanesViewHolder holder, int position) {
        //Aca trabajamos con el recyclerView
        final Plan plan = planes.get(position);
        holder.tvNombrePlan.setText(plan.getNombre().toUpperCase());

        if (plan.getImagen().equals("1")){
            holder.ivPlan.setImageResource(R.drawable.buff_academy);
        } else if(plan.getImagen().equals("2")){
            holder.ivPlan.setImageResource(R.drawable.img2);
        } else if(plan.getImagen().equals("3")){
            holder.ivPlan.setImageResource(R.drawable.athleanx);
        } else if(plan.getImagen().equals("4")){
            holder.ivPlan.setImageResource(R.drawable.img4);
        } else if(plan.getImagen().equals("5")){
            holder.ivPlan.setImageResource(R.drawable.img5);
        } else if(plan.getImagen().equals("6")){
            holder.ivPlan.setImageResource(R.drawable.img6);
        } else if(plan.getImagen().equals("7")){
            holder.ivPlan.setImageResource(R.drawable.img7);
        } else if(plan.getImagen().equals("8")){
            holder.ivPlan.setImageResource(R.drawable.img8);
        } else if(plan.getImagen().equals("9")){
            holder.ivPlan.setImageResource(R.drawable.img9);
        } else if(plan.getImagen().equals("10")){
            holder.ivPlan.setImageResource(R.drawable.img10);
        } else if(plan.getImagen().equals("11")){
            holder.ivPlan.setImageResource(R.drawable.img11);
        } else if(plan.getImagen().equals("12")){
            holder.ivPlan.setImageResource(R.drawable.img12);
        } else if(plan.getImagen().equals("13")){
            holder.ivPlan.setImageResource(R.drawable.img13);
        } else if(plan.getImagen().equals("14")){
            holder.ivPlan.setImageResource(R.drawable.img14);
        } else if(plan.getImagen().equals("15")){
            holder.ivPlan.setImageResource(R.drawable.img15);
        } else if(plan.getImagen().equals("16")){
            holder.ivPlan.setImageResource(R.drawable.img16);
        } else if(plan.getImagen().equals("17")){
            holder.ivPlan.setImageResource(R.drawable.img17);
        } else if(plan.getImagen().equals("18")){
            holder.ivPlan.setImageResource(R.drawable.img18);
        }

        //holder.ivPlan.setImageDrawable();
        holder.buttonPlan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), TrainingActivity.class);
                intent.putExtra("planUsuario", plan.getImagen());
                v.getContext().startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return planes.size();
    }

    public static class PlanesViewHolder extends RecyclerView.ViewHolder {

        TextView tvNombrePlan;
        ImageView ivPlan;
        Button buttonPlan;

        public PlanesViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNombrePlan = (TextView) itemView.findViewById(R.id.tvNombrePlanRecycler);
            ivPlan = (ImageView) itemView.findViewById(R.id.ivPlanRecycler);
            buttonPlan = itemView.findViewById(R.id.bEntrenarRecycler);
        }
    }


}
