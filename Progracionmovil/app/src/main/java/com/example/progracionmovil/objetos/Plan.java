package com.example.progracionmovil.objetos;

public class Plan {
    private String nombre, imagen;

    public Plan(String nombre, String imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public Plan() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
