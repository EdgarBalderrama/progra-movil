package com.example.progracionmovil.objetos;

public class Usuario {
    private String altura, edad, entrenamiento, genero, nivel, objetivo, peso, imcUsuario, plan;

    public Usuario() {
    }

    public Usuario(String altura, String edad, String entrenamiento, String genero, String nivel, String objetivo, String peso, String imcUsuario, String plan) {
        this.altura = altura;
        this.edad = edad;
        this.entrenamiento = entrenamiento;
        this.genero = genero;
        this.nivel = nivel;
        this.objetivo = objetivo;
        this.peso = peso;
        this.imcUsuario = imcUsuario;
        this.plan = plan;
    }

    public String getAltura() {
        return altura;
    }

    public void setAltura(String altura) {
        this.altura = altura;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getEntrenamiento() {
        return entrenamiento;
    }

    public void setEntrenamiento(String entrenamiento) {
        this.entrenamiento = entrenamiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getObjetivo() {
        return objetivo;
    }

    public void setObjetivo(String objetivo) {
        this.objetivo = objetivo;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getImcUsuario() {
        return imcUsuario;
    }

    public void setImcUsuario(String imcUsuario) {
        this.imcUsuario = imcUsuario;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }
}
