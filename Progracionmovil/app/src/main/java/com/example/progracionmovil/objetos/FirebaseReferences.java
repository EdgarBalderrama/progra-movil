package com.example.progracionmovil.objetos;

public class FirebaseReferences {

    //Ejercicio
    final public static String TRAINING_REFERENCE = "prueba";
    final public static String EJERCICIO_REFERENCE = "ejercicio";
    final public static String PLAN_BD_REFERENCE = "plan";
    final public static String NUMERO_REFERENCE = "Ejer1";


    //Usuario
    final public static String USUARIO_REFERENCE = "usuario";
    public static String ID_REFERENCE = "";
    public static String OBJETIVO_REFERENCE = "";
    public static String ENTRENAMIENTOS_REFERENCE = "";
    public static String GENERO_REFERENCE = "";
    public static String NIVEL_REFERENCE = "";
    public static String EDAD_REFERENCE = "";
    public static String ALTURA_REFERENCE = "";
    public static String PESO_REFERENCE = "";
    public static String IMC_REFERENCE = "";
    public static String PLAN_REFERENCE = "";

}
