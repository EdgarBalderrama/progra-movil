package com.example.progracionmovil.objetos;

public class Ejercicios {
    private String descripcion, imagen, nombre;
    private String numeroCiclos, numeroRepeticiones;
    private String tipo;

    public Ejercicios(String descripcion, String imagen, String nombre, String numeroCiclos, String numeroRepeticiones, String tipo) {
        this.descripcion = descripcion;
        this.imagen = imagen;
        this.nombre = nombre;
        this.numeroCiclos = numeroCiclos;
        this.numeroRepeticiones = numeroRepeticiones;
        this.tipo = tipo;
    }

    public Ejercicios(){

    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNumeroCiclos() {
        return numeroCiclos;
    }

    public void setNumeroCiclos(String numeroCiclos) {
        this.numeroCiclos = numeroCiclos;
    }

    public String getNumeroRepeticiones() {
        return numeroRepeticiones;
    }

    public void setNumeroRepeticiones(String numeroRepeticiones) {
        this.numeroRepeticiones = numeroRepeticiones;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
