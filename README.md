# Best Home Workout

### Description 🧑‍💻

It is an adaptable and customizable home training mobile application.

### Characteristics 💪

- Structure a personalized routine according to the objective, current level and equipment that the user has.

- General listing of many exercises besides users routine

- Detailed explanation of the form and technique of each exercise

- Sample animations for each exercise

- User ability to manage their training items

- Calculation of the user's BMI

- Stopwatch for each workout

